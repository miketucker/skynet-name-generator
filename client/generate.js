
Template.generate.ideas = function(){
	if(Session.get('showRejects')){
		return Ideas.find({});
	} else {
		return Ideas.find({status: 1});
	}

}

Template.generate.companies = function(){
	return Companies.find({},{sort: {votes: -1}});
}

function toTitleCase(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

Template.generate.sessionPrefix = function(){
	return Session.get('prefix');
}

Template.generate.sessionSuffix = function(){
	return Session.get('suffix');
}

Template.generate.capitalize = function(word){
	return toTitleCase(word);
}

var addNames = function(count){

	var p = Session.get('prefix');
	var s = Session.get('suffix');
	var prefixes, suffixes;

	if(p && p != '') prefixes = [{name:p}];
	else prefixes = Words.find({type:'prefix'},{sort:{votes:-1}}).fetch();
	
	if(s && s != '') suffixes = [{name:s}];
	else suffixes = Words.find({type:'suffix'},{sort:{votes:-1}}).fetch();	

	// console.log(prefixes,suffixes);
	var list = [];

	for (var i = count; i >= 0; i--) {
		var pre = prefixes[_.random(0,prefixes.length-1)];
		var suf = suffixes[_.random(0,suffixes.length-1)];
		if(pre && suf){
			var name = pre.name + " " + suf.name;
			if(!_.contains(list,name)){
				list.push(name);
				// Meteor.call('addIdea',name);
			}
		}
	};

	Session.set('list',list);
}

Template.generate.sessionList = function(){
	return Session.get('list');
}

Template.generate.addNames = addNames;

Template.generate.events({

	'submit form.addTitle': function(e){
		// console.log(e.target[0]);
		Meteor.call("addCompany",e.target[0].value);
		e.preventDefault();
	},

	"click .generated a": function(e){
		Meteor.call('addCompany',(e.target.attributes['data'].value));
	},

	"click #refresh": function(e){
		addNames(20);
	},

	"click #randomize": function(e){
		Session.set('prefix',null);
		Session.set('suffix',null);
	},

	"click #generate": function(e){
		addNames(50);
	}


});

Template.generate.sessionIsSet = function(){
	return (Session.get('prefix') || Session.get('suffix'));
}

Template.idea.capitalize = function(word){
	return toTitleCase(word);
}

Template.idea.events({
	"click .add": function(e){
		Meteor.call('addCompany',this);
		e.preventDefault();
	},

	"click .reject": function(e){
		console.log(this);
		Meteor.call('rejectCompany',this._id);
		e.preventDefault();
	}
});


/* ------------------------ COMPANY NAMES -------------------- */

Template.companyName.capitalize = function(word){
	return toTitleCase(word);
}

Template.companyName.handleCompany = function(name){
	var words = name.split(' ');

	var r = "";
	var kind = 'suffix';

	for (var i = 0; i < words.length; i++){
		kind = (i==0) ? 'prefix' : 'suffix';
		r += "<a href='#' data-kind='"+kind+"' data-val='"+words[i]+"'>"+toTitleCase(words[i])+"</a> ";
	}

	return r;
}


Template.companyName.events({
	"click .title a": function(e){

		Session.set(e.target.attributes['data-kind'].value,e.target.attributes['data-val'].value);
		console.log('e',e.target.attributes['data-kind'],e.target.attributes['data-val']);
	},

	"click .minus": function(e){
		// console.log(this._id);
		Meteor.call('downvoteCompany',this._id);
	},
	"click .plus": function(e){
		// console.log(this._id);
		Meteor.call('upvoteCompany',this._id);
	}
});
