
var api_key = '25b39037cd729a7e6900507fd800fc93143ac5ee63a64c2b8';

Template.lists.prefixes = function(){
	return Words.find({type:'prefix'},{sort:{votes:-1}});
}

Template.lists.suffixes = function(){
	return Words.find({type:'suffix'},{sort:{votes:-1}});
}

function getSyns(word){

	$('#searchTerm').html(": " + word);

	$.get("http://api.wordnik.com:80/v4/word.json/"+word+"/relatedWords?useCanonical=false&relationshipTypes=synonym&limitPerRelationshipType=10&api_key="+api_key, 
		function(res) {
			// console.log('res',res);
			if(res.length > 0){
				var words = res[0].words;
				var resultList = $('.result-list');
				resultList.html('');

				for (var i = words.length - 1; i >= 0; i--) {
					resultList.append("<div class='word' data-word='"+words[i]+"'><a href='#' class='search'>?</a> <a href='#' class='prefix'>pre</a> <a href='#' class='suffix'>suf</a> "+words[i] + "<br/>");
				};
			}
		}
	);

}


Template.lists.events({

	'submit form.addPrefix': function(e){
		Meteor.call("addPrefix",e.target[0].value);
		e.preventDefault();
	},

	'submit form.addSuffix ': function(e){
		Meteor.call("addSuffix",e.target[0].value);
		e.preventDefault();
	},

	'click .word-list a': function(e){
		// console.log('hi',e.target.innerText);
		getSyns(e.target.innerText);
		e.preventDefault();
	},

	'click .result-list .word .search': function(e){
		e.preventDefault();
		var v = e.target.parentNode.attributes['data-word'].value;
		getSyns(v);
		// getSyns();
	},

	'click .result-list .word .prefix': function(e){
		var word = e.target.parentNode.attributes['data-word'].value;
		Meteor.call('addPrefix',word);
	},

	'click .result-list .word .suffix': function(e){
		var word = e.target.parentNode.attributes['data-word'].value;
		Meteor.call('addSuffix',word);
	}


});