Router.configure({
	layoutTemplate: "masterLayout"
});

Router.map(function(){

	this.route('generate',{
		path: '/',
		onBeforeAction: function(){
			this.subscribe("prefixes");
			this.subscribe("suffixes");
			this.subscribe('companies');
			
			pres = Words.find({type:'prefix'},{sort:{votes:-1}});
			suf = Words.find({type:'suffix'},{sort:{votes:-1}});
		}
		
	});


	this.route('lists', {
		onBeforeAction: function(){
			this.subscribe("prefixes");
			this.subscribe("suffixes");


		}

	});

});