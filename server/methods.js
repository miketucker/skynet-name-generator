Meteor.methods({

	addPrefix: function(word){
		if(Words.find({name: word, type:"prefix"}).count() < 1){
			Words.insert({
				name: word,
				gen: 0,
				votes: 0,
				type: 'prefix'
			});
		}
	},

	addSuffix: function(word){

		if(Words.find({name: word, type:"suffix"}).count() < 1){
			Words.insert({
				name: word,
				gen: 0,
				votes: 0,
				type: 'suffix'
			});
		}
	},

	addCompany: function(name){
		Companies.insert({
			name: name,
			votes: 0
		});

		// Ideas.update(obj._id,
		// 	{status: 2}
		// );
	},

	rejectCompany: function(id){
		// console.log('reject',_id);
		Ideas.update(id,
			{status: 0}
		);
	},

	addIdea: function(name){
		if(Ideas.find({name: name}).count() < 1){
			Ideas.insert({
				name: name,
				status: 1
			});
		}
	},

	upvoteCompany: function(id){
		Companies.update(
			{ _id: id },
			{
				$inc: { votes: 1 }
			}
		);
	},

	downvoteCompany: function(id){
		var c = Companies.findOne(id);

		console.log('downvote',c.votes);
		if(c.votes <= 0){
			Companies.remove(id);
		} else {
			Companies.update(
				{ _id: id },
				{
					$inc: { votes: -1 }
				}
			);
		}
	}

});