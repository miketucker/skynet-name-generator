

Meteor.publish('prefixes', function () {
	return Words.find({"type":"prefix"},{sort:{votes: -1}});
});

Meteor.publish('suffixes', function () {
	return Words.find({"type":"suffix"},{sort:{votes: -1}});
});

Meteor.publish('ideas', function () {
	return Ideas.find({});
});

Meteor.publish('companies', function () {
	return Companies.find({});
});